use std::rc::Rc;

pub type CommandReturnType<'a> = Result<String, &'a str>;
pub trait CommandCallback {
    fn call(&self, path: String, args: Vec<String>, preceding: Option<String>) -> CommandReturnType;
}

pub type NamedCallback = (String, Box<dyn CommandCallback>);

#[macro_export]
macro_rules! callback {
    (($name:expr, $fun:expr)) => {
        {
            use std::rc;

            let mut value: pluglib::NamedCallback;
            struct TempCallback { };
            impl $crate::CommandCallback for TempCallback {
                fn call(&self, path: String, args: Vec<String>, preceding: Option<String>) -> $crate::CommandReturnType {
                    $fun(path, args, preceding)
                }
            };

            value = ($name.to_string(), Box::new(TempCallback{ }));
            value
        }
    };

    ($fn: ident) => {
        $crate::callback!((stringify!($fn), $fn))
    };
}

#[macro_export]
macro_rules! matcher {
    ($fn: ident) => {
        $crate::callback!(("match", $fn))
    };
}

#[derive(Debug)]
pub struct PluginDefinition {
    pub rustc_version: &'static str,
    pub core_version: &'static str,

    //The name of the plugin
    pub name: String,
    //The containing namespace
    pub namespace: String,
}

impl PluginDefinition {
    pub fn full_name(&self) -> String {
        let mut res = "".to_string();
        res.push_str(&self.namespace);
        res.push_str("/");
        res.push_str(&self.name);

        res
    }
}

//Macro for generating the static function that will be called upon initialization
#[macro_export]
macro_rules! export_plugin {
    ($name:expr, $namespace:expr, $($callback:expr),*) => {
        #[doc(hidden)]
        #[no_mangle]
        pub extern "C" fn register_plugin() -> ($crate::PluginDefinition, Vec<$crate::NamedCallback>) {
        // pub extern "C" fn register_plugin() -> $crate::PluginDefinition {
            let name: String = $name.to_string();
            let namespace: String = $namespace.to_string();
            // let callbacks: Vec<NamedCallback> = $callbacks;

            let plugin_definition: $crate::PluginDefinition = $crate::PluginDefinition {
                rustc_version: "filled",
                core_version: "filled",

                name: name,
                namespace: namespace,
            }; 

            let mut callbacks: Vec<$crate::NamedCallback> = vec![];

            $( 
                callbacks.push($callback); 
            )*

            return (plugin_definition, callbacks);
            // return plugin_definition;
        }
    }
}


#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
