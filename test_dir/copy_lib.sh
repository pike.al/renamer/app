#! /bin/sh

PLUG_DIR="../target/debug/"

PLUGINS=${@:-core_plugin}

[ -x "./plugins" ] || exit "Plugin directory not found here." 1

for p in $PLUGINS; do
    echo Copying plugin $p
    plugin_name=lib$p.so
    rm -f plugins/$plugin_name
    cp $PLUG_DIR/$plugin_name ./plugins/$plugin_name
done
