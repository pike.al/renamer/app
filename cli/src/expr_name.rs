use std::path::PathBuf;
use nom:: {
    IResult,
    bytes::complete::{tag, take_till, take_until, take_while, take_while1, is_not},
    branch::{alt},
    combinator::{map, map_parser, eof, rest},
    sequence::{delimited, tuple, preceded},
    multi::{separated_list0, separated_list1, many0, many1, many_till}
};

use crate::ns::Namesite;

#[derive(Debug)]
pub struct FunctionCall {
    pub name: String,
    pub args: Vec<String>
}

impl FunctionCall {
    fn call<'a>(&'a self, ns: &'a Namesite, path: &str, last_val: Option<String>) -> Result<String, &str> {
        let func = ns.get(&self.name.trim())?;
        if let None = func {
            return Err("Func not found");
        }

        func.unwrap().call(path, self.args.clone(), last_val.clone())
    }
}

#[derive(Debug)]
pub struct SquareExp {
    pub chain: Vec<FunctionCall>
}

impl SquareExp {
    fn call<'a>(&'a self, path: &str, ns: &'a Namesite, last_val: String) -> Result<Option<String>, &str> {
        let mut last_val: Option<String> = Some(last_val);

        for f in &self.chain {
            last_val = Some(f.call(ns, path, last_val)?);
        };

        Ok(last_val)
    }
}

#[derive(Debug)]
pub enum MatchTerm {
    Literal(String),
    InsideSquares(SquareExp)
}

pub fn parse_function_call(input: &str) -> IResult<&str, FunctionCall> {
    let (input, (function_name, args)) = tuple((
            take_while1(|c: char| c != '('),
            alt((delimited(
                tag("("), 
                    take_until(")"),
                tag(")")), eof))
        ))(input)?;

    let (_,args) = separated_list0(tag(","), take_while(|c: char| c != ','))(args)?;


    return Ok((input, FunctionCall {
        name: function_name.to_string(),
        args: args.iter().map(|s| s.to_string()).collect()
    }))
}

pub fn parse_square_brackets(input: &str) -> IResult<&str, MatchTerm> {
    let (input, results) = separated_list1(
            tag("|"),
            map_parser(take_while(|c: char| c != '|'), parse_function_call)
        )(input)?;

    Ok((input, 
        MatchTerm::InsideSquares(SquareExp {
            chain: results
        })))
}

pub fn parse_matchterm(input: &str) -> IResult<&str, MatchTerm> {
    if input == "" {
        return Ok((input, MatchTerm::Literal("".to_string())));
    }
 
    //There are two cases, either we have a SquareExp, or a Literal:
    //For the sake of simplicity, we dealt with empty strings above.
    if input.chars().nth(0).unwrap() == '[' {
        //We are dealing with a SquareExp: 
        let (input, result) = delimited(
            tag("["),
            map_parser(take_while(|c| c != ']'), parse_square_brackets), 
            tag("]"))(input)?;

        return Ok((input, result));
    } else {
        //Literal:
        let (input, result) = map(take_while(|c| c != '['), |s: &str| MatchTerm::Literal(s.to_string()))(input)?;

        return Ok((input, result));
    }
}

pub fn parse_filename_exp(input: &str) -> IResult<&str, Vec<MatchTerm>> {
    let (input, (result, _)) = preceded(tag("\t"), many_till(parse_matchterm, eof))(input)?;

    return Ok((input, result));
}

pub fn evaluate_filename(path: PathBuf, filename_exp: String, root_ns: &Namesite) -> Result<String, &str> {
    let path_str = path.to_str().unwrap().to_string();
    let (_, parsed) = parse_filename_exp(&filename_exp.clone()).or(Err("Error file_exp"))?;

    let evaluated = parsed.iter().map( move |match_term| -> Result<String, &str> {
        match &match_term {
            MatchTerm::Literal(l) => {
                Ok(l.to_string()) 
            },
            MatchTerm::InsideSquares(square_exp) => {
                let result = square_exp.call(&path_str, root_ns, (&filename_exp).to_string())
                    .or_else(|e| {
                        println!("{}", e);
                        Err("There was an error!")
                    })?; //Notice!!
                match result {
                    Some(s) => Ok(s.clone()),
                    None => Ok("".to_string())
                }
            }
        }
    }).collect::<Result<Vec<String>, &str>>()?;

    return Ok(evaluated.join(""));
}
