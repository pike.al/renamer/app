use std::cell::{RefCell};
use std::rc::Rc;
use std::collections::HashMap;
use std::ops;

use pluglib::{CommandReturnType, CommandCallback};
use crate::loader::CallbackWrapper;

pub enum NamesiteKind {
    Command(CallbackWrapper),
    // Namespace(RefCell<HashMap<String, Namesite>>)
    Namespace(Box<RefCell<Vec<Namesite>>>)
}

pub struct Namesite {
    pub name: String,
    pub plugin: Option<String>,
    pub kind: NamesiteKind
}

impl<'a> Namesite {
    pub fn call(&self, path: &str, args: Vec<String>, preceding: Option<String>) -> CommandReturnType {
        match &self.kind {
            NamesiteKind::Command(callback) => {
                Ok(callback.call(path.to_string(), args, preceding)?)
            },
            NamesiteKind::Namespace{ .. } => {
                //Get the match namesite. If error, propagate, otherwise unwrap, in which case
                //if it doesn't exist, return an error. If all goes well call then callback
                //and return value
                match &self
                    .get("match")?
                    .ok_or("Match rule not found!")?
                    .kind {
                        NamesiteKind::Command(callback) => {
                            Ok(callback.call(path.to_string(), args, preceding)?)
                        }
                        _ => Err("\"match\" is not a plugin function, rather a namespace.")
                }
            }
        }
    }

    pub fn get(&self, name: &str) -> Result<Option<&Namesite>, &str> {
        // println!("Looking up {} in {}", name, self.name);
        match &self.kind {
            NamesiteKind::Command{ .. } => Err("Commands are not addressable"),
            NamesiteKind::Namespace(sites) => {
                let mut namespace_path = name.split('/');

                let mut first = namespace_path.next().unwrap();
                //In case we have the "" namespace, then we skip this altogether, since "" is the
                //root of the current Namesite, so there'd be no point to get("")

                if first == "" {
                    match namespace_path.next() {
                        Some(n) => first = n,
                        None => return Ok(Some(&self))
                    }; 
                }
                let rest = namespace_path.collect::<Vec<&str>>().join("/");

                let mut result: Option<&Namesite> = None;

                //unsafe {
                //    //This should techincally be safe, after all, I want to get an
                //    //immutable reference from a RefCell,
                //    let ptr: *mut HashMap<_,_> = sites.as_ptr();

                //    result = (*ptr).get(first);
                //}
                // for ns in (**sites).i() {
                //     // if ns.name == first {
                //     //     result = Some(&ns);
                //     // }
                // }
                // let mut _mutable = sites.borrow_mut();
                unsafe {
                    let ptr: *mut Vec<_> = sites.as_ptr();
                    result = (*ptr).iter().find(|s| s.name == first);
                }

                //Nested calls
                match result {
                    Some(x) => {
                        if rest != "" {
                            return x.get(&rest);
                        } else {
                            return Ok(result);
                        }
                    },
                    None => {
                        // println!("asdfadsfas: {}", name);
                        // Err("There was an error while looking up namepoint!")
                        Ok(None)
                    }
                }
            }
        }
    }

    //Get a namespace, or return a created one
    pub fn get_or_create(&self, name: &str, plugin: Option<String>) -> Result<&Namesite, &str> {
        // println!("Creating namespace {}", name);

        let mut last_found: &Namesite = &self;

        for n in name.split('/') {
            let found = last_found.get(n)?;
            let found = &found;

            last_found = match found {
                Some(ns) => ns,
                None => {
                    last_found.insert(Namesite::new_namespace(n, plugin.clone()))? //Notice this!!!
                }
            };
        };
        // println!("Created namespace {}", last_found.name);

        Ok(last_found)
    }

    pub fn insert(&self, site: Namesite) -> Result<&Namesite, &str> {
    
        match &self.kind {
            NamesiteKind::Command{ .. } => return Err("Commands are not addressable"), 
            NamesiteKind::Namespace(sites) => {
                let name = site.name.clone();

                sites.borrow_mut().push(site);
                return Ok(self.get(&name)?.unwrap());
            }
        }
    }

    pub fn clear(&mut self) {
        match &self.kind {
            NamesiteKind::Namespace(sites) => {
                sites.borrow_mut().clear();
            }
            _ => {}
        }
        
    }

    pub fn new_namespace(name: &str, plugin: Option<String>) -> Namesite {
        Namesite {
            name: name.to_string(),
            plugin: plugin,
            kind: NamesiteKind::Namespace(Box::new(RefCell::new(vec![])))
        }
    }

    pub fn new_callback(name: &str, callback: CallbackWrapper, plugin: Option<String>) -> Namesite {
        Namesite {
            name: name.to_string(),
            plugin: plugin,
            kind: NamesiteKind::Command(callback)
        }
    }
}
