use std::path::PathBuf;
use structopt::StructOpt;
use pluglib;

mod config;
mod loader;
mod expr_rule;
mod ns;
mod expr_name;
mod rename;

//
use std::alloc::System;

#[global_allocator]
static ALLOCATOR: System = System;
//

#[derive(StructOpt)]
struct Cli {
    #[structopt(short, default_value="~", long, parse(from_os_str))]
    config: PathBuf,

    #[structopt(short, long, parse(from_os_str))]
    plugins: PathBuf,

    #[structopt(name = "WORKDIR", default_value=".", parse(from_os_str))]
    workdir: PathBuf
}

fn main() {
    let mut opt = Cli::from_args();

    let config_file = opt.config.join("Renamefile");

    let cfg = config::read_config(config_file); 
    if let Err(_) = cfg {
        eprintln!("Error reading config file!");
        return;
    }

    let cfg = cfg.unwrap();

    let mut root_namespace = ns::Namesite::new_namespace(
        "",
        None
    );

    //Print the config
    // println!("{:#?}", cfg);

    //Namespace stuff
    let mut plugin_loader = loader::PluginLoader::new(opt.plugins);
    match plugin_loader.load_into_ns(&root_namespace) {
        Err(e) => {
            eprintln!("{}", e);
            return;
        },
        _ => {}
    }

    for folder_block in &cfg {
        let result_map = rename::apply_folder_block(&folder_block, &root_namespace);
        match result_map {
            Err(_e) => {}, //eprintln!("{}", e),
            Ok(vecs_of_tuples) => {
                //Do the renaming here:
                for tuples in &vecs_of_tuples {
                    //Finally do the renaming
                    let result = rename::apply_renames(tuples);
                }
            }
        }
    }

    root_namespace.clear();
    // Not sure why it does, but it does work with this only:
}


