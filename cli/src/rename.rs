use std;
use std::fs;
use std::path::PathBuf;

use crate::config;
use crate::ns;

pub fn apply_folder_block(block: &config::FolderBlock, root_ns: &ns::Namesite) 
    -> Result<Vec<Vec<(PathBuf, String)>>, &'static str> {
    let (results, failed): (Vec<Result<_,_>>, Vec<_>) = block.paths.iter()
        .map( |p| iterate_dir(p.clone(), block.recursive) )
        .partition(|r| r.is_ok() );

    if failed.len() > 0 {
        return Err("Error iterating over files");
    }

    let mapped_results: Vec<Vec<(PathBuf, String)>> = results.iter().map(|r| {
        r.as_ref().unwrap().iter().map(|p| {
            let result = block.matching_rule(p.to_path_buf(), root_ns).unwrap();

            if let Some(rule) = result {
                println!("Matching rule: {}", rule.filename);
                return rule.apply(p.to_path_buf(), root_ns).unwrap();
            }

            //Do the transformation here and return (p, transformed)
            (
                p.clone(), 
                p.file_name().unwrap()
                    .to_str().unwrap()
                    .to_string()
            ) //Wow

        }).collect()
    }).collect();

    // println!("{:?}", mapped_results);

    Ok(mapped_results)
}

pub fn iterate_dir(path: PathBuf, recurse: bool) -> Result<Vec<PathBuf>, &'static str>{
    if !path.is_dir() {
        return Err("Specified path is not directory!");
    }

    let mut results: Vec<PathBuf> = vec![];

    for entry in fs::read_dir(path).or(Err(""))?{
        let entry = entry;

        if let Err(_) = entry {
            return Err("There was an error loading directory entries!");
        }
        let entry = entry.unwrap();

        let path = entry.path();
        if path.is_dir() && recurse {
            results.extend(iterate_dir(path, recurse)?);
        } else if !path.is_dir() {
            results.push(path);
        }
    }

    Ok(results)
}

pub fn apply_renames(pairs: &Vec<(PathBuf, String)>) -> Result<(), &'static str> {
    println!("Total files to be renamed: {}", pairs.len());
    for (path, new_name) in pairs {
        let new_path = path.parent().unwrap().join(new_name);
        // println!("old path: {} ; new_path: {}", path.to_str().unwrap(), new_path.to_str().unwrap());
        if !path.is_dir() && !path.eq(&new_path) {
            println!("Moving {} to {}", path.to_str().unwrap(), new_path.to_str().unwrap());
            fs::rename(path, new_path).or(Err("There was a problem renaming file!"))?;
        }
    }

    Ok(())
}
