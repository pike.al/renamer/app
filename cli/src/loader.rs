use std::rc::Rc;
use std::fs::{ReadDir, read_dir};
use std::path::PathBuf;
use libloading::{Library, Symbol};
use pluglib;

use crate::ns;

pub struct PluginLoader {
    path: PathBuf,
    libraries: Vec<Rc<Library>>,
    pub loaded_plugins: Vec<Rc<pluglib::PluginDefinition>>
}

//Calls the given plugin, for given name!
pub struct CallbackWrapper {
    name: String,
    library: Rc<Library>,
    plugin: Rc<pluglib::PluginDefinition>,
    callback: Box<dyn pluglib::CommandCallback>
}

impl CallbackWrapper {
    pub fn new(name: String, library: Rc<Library>, plugin: Rc<pluglib::PluginDefinition>, callback: Box<dyn pluglib::CommandCallback>)
        -> CallbackWrapper {
        CallbackWrapper {
            name: name,
            library: library,
            plugin: plugin,
            callback: callback
        }
    }
}

impl pluglib::CommandCallback for CallbackWrapper {
    fn call(&self, path: String, args: Vec<String>, preceding: Option<String>) 
        -> pluglib::CommandReturnType {
        self.callback.call(path, args, preceding)
    }
}

impl<'a> PluginLoader {
    pub fn new(path: PathBuf) -> PluginLoader {
        PluginLoader {
            path: path.clone(),
            libraries: vec![],
            loaded_plugins: vec![]
        }
    }

    pub fn load_into_ns(&'a mut self, root_ns: &'a ns::Namesite) -> Result<(), &str> {
        let files = read_dir(&self.path).or(Err("Could not load plugin"))?;

        for file in files { 
            let name: String = file
                .or(Err("IO Error"))?
                .file_name()
                .into_string().unwrap();

            //Load the plugin here:
            let library = Rc::new(unsafe { Library::new(self.path.join(name.clone())) }.or(Err("Could not load lib"))?);
            println!("Loading plugin: {}", name.clone());
            self.libraries.push(Rc::clone(&library));

            type PluginRegister = unsafe fn() -> (pluglib::PluginDefinition, Vec<pluglib::NamedCallback>);
            // type PluginRegister = unsafe fn() -> pluglib::PluginDefinition;
            let load_func: Symbol<PluginRegister> = unsafe { library.get(b"register_plugin") }.or(Err(""))?;

            //Actually load the plugin
            let some_res = unsafe { load_func() } ;
            let plugin_rc = Rc::new(some_res.0);
            self.loaded_plugins.push(Rc::clone(&plugin_rc));

            // println!("definition is {:#?}", plugin_rc.full_name());

            //Create a namespace for the plugin:
            let ns = root_ns.get_or_create(&plugin_rc.full_name(), Some(plugin_rc.name.clone()))?;

            let callbacks = some_res.1;

            for (name, callback) in callbacks {
                //Wrap the callback
                // println!("Registering callback: {}", name.clone());

                let wrapper = CallbackWrapper::new(
                    name.clone(),
                    Rc::clone(&library),
                    Rc::clone(&plugin_rc),
                    callback
                );

                //Create a namepoint, and add it to the namespace
                let additional_ns = ns::Namesite::new_callback(&(name.clone()), wrapper, Some(name.clone())); 
                // println!("{}", additional_ns.call("", vec!["hello".to_string()], Some("asdfadf".to_string())).unwrap());
                ns.insert(additional_ns);
            }
        }
        Ok(())
    }

    // pub unsafe fn load_plugin(&mut self, name: &str) -> Result<pluglib::PluginDefinition, &str> {
    //     let library = Rc::new(Library::new(self.path.join(name)).or(Err("Could not load lib"))?);
    //     println!("Loading plugin: {}", name);
    //     self.libraries.push(Rc::clone(&library));

    //     // match &library {
    //     //     Err(e) => {
    //     //         println!("{}", e);
    //     //     },
    //     //     _ => {}
    //     // };

    //     // let library = library.or(Err("Could not load lib"))?;
    //     type PluginRegister = unsafe fn() -> pluglib::PluginDefinition;

    //     let load_func: Symbol<PluginRegister> = library.get(b"register_plugin").or(Err(""))?;
    //     println!("Hello!");

    //     let some_res: pluglib::PluginDefinition = load_func();
    //     println!("{}", &some_res.rustc_version);

    //     Ok(some_res)
    //     // Ok(Box::from_raw(load_func()))
    // }

    //pub fn load_plugins(&mut self) -> Result<(), &str> {
    //    let files = read_dir(&self.path).or(Err("Could not load plugin"))?;
    //    for file in files { 
    //        let file_name: String = file
    //            .or(Err("IO Error"))?
    //            .file_name()
    //            .into_string().unwrap();

    //        println!("{}", file_name.clone());

    //        let res = unsafe { self.load_plugin(&file_name) };

    //        match res {
    //            Ok(plug) => {
    //                if self.loaded_plugins.iter().any ( |p| p.full_name() == plug.full_name() ) {
    //                    return Err("Plugin namespace already in use. There's collision!");
    //                }

    //                // println!("{}", plug.name.clone());

    //                self.loaded_plugins.push(plug);
    //            },
    //            Err(e) => {
    //                println!("{}", e);
    //                return Err("Error!")
    //            }
    //        };
    //    }

    //    Ok(())
    //}

    //// Adds the functions of a plugin to a given namespace
    //pub fn populate_namespace(&'a mut self, namesite: &'a ns::Namesite<'a>) -> Result<(), &str> {
    //    println!("Loaded plugins count: {}", self.loaded_plugins.len());
    //    for plug in self.loaded_plugins.iter() {
    //        let ns = namesite.get_or_create(&plug.name, Some(plug.name.clone()))?;

    //        //This sort of unloads the plugins, but couldn't come up with anything better.
    //        //Maybe should have used Rc instead of boxes
    //        for named_func in &plug.callbacks {
    //            // let (name,func) = named_func;
    //            println!("!!!");

    //            // let fw = FunctionWrapper {
    //            //     callback_name
    //            // };
    //            // let additional_ns = ns::Namesite::new_callback(&name, &named_func.1, Some(plug.name.clone())); 

    //            // if name == "length" {
    //            //     println!("{}", name);
    //            //     println!("{}", func.call("".to_string(), vec!["hello".to_string()], Some("asdfadf".to_string())).unwrap());
    //            // }

    //            // ns.insert(additional_ns)?;
    //        }

    //        println!("bbb");
    //    }

    //    Ok(())
    //}
}
