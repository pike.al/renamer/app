use regex::Regex;
use std::collections::HashMap;
use eval::Expr;
use crate::ns::Namesite;

/// Function to retrieve a matcher plugin call as a closure.
///
/// A matcher plugin call is a function that gets a filename and returns a boolean that indicates
/// whether the filename matches its rules.
///
/// # Arguments
///
/// * `namesite` - Namesite from which we access plugins.
/// * `plugin_path` - The plugin path, excluding the function name (which is always 'matches').
///
/// # Returns
///
/// A closure that represents a plugin call
///
/// # Example
///
/// let namesite = /* get namesite */;
/// let plugin_call = get_matcher_plugin_call(namesite, "amazon/my_invoice");
/// let current_filename = "/tmp/some_file.pdf";
/// let result = plugin_call(current_filename); // This will execute the plugin call
///
fn get_matcher_plugin_call<'a>(
    namesite: &'a Namesite,
    plugin_path: String,
) -> Box<dyn Fn(String) -> bool + 'a> {
    Box::new(
        move |filename: String| -> bool {
            // println!("{} {}", plugin_path, filename);

            namesite.get(&plugin_path)
                .unwrap()
                .unwrap()
                .call(&filename, vec![], None)
                .unwrap()
                .eq("true")
        }
    )
}

/// Function to retrieve a plugin call dispatcher for a namesite
/// (Basically a wrapper for get_plugin_call that satisfies the namesite argument)
///
/// # Arguments
///
/// * `namesite` - Namesite from which we access plugins
///
/// # Returns
///
/// A closure that given a plugin_path returns the plugin as a closure
///
/// # Example
///
/// let plugin_call_dispatcher = get_plugin_call_dispatcher(namesite)
/// let plugin_call = plugin_call_dispatcher("amazon/my_invoice/matches");
///
fn get_plugin_call_dispatcher<'a>(
    namesite: &'a Namesite,
) -> Box<dyn Fn(String) -> Box<dyn Fn(String) -> bool + 'a> + 'a> {
    Box::new(
        move |plugin_path: String| -> Box<dyn Fn(String) -> bool + 'a> {
            get_matcher_plugin_call(namesite, plugin_path)
        }
    )
}

/// Function to generate a rule expression matcher closure.
///
/// Expressions are a series of matcher plugin calls.
/// You can use the result of this function to evaluate an expression based on the
/// filename you provide.
///
/// # Arguments
///
/// * `expression` - The rule expression that needs to be evaluated
/// * `call_dispatcher` - A function that retrieves matcher plugin calls from their name
///
/// # Returns
///
/// A closure that takes a filename and evaluates it against the set rule matcher expression
///
/// # Example
///
/// let plugin_call_dispatcher = get_plugin_call_dispatcher(namesite)
/// let expression = "!amazon/amazon_invoice && freddy/amazon"
/// let matcher = get_expression_matcher(expression, plugin_call_dispatcher)
/// let current_filename = "/tmp/some_file.pdf";
/// let result = matcher(current_filename); // This will evaluate the entire expression
///
pub fn get_expression_matcher<'a>(
    expression: String,
    call_dispatcher: Box<dyn Fn(String) -> Box<dyn Fn(String) -> bool + 'a> + 'a>,
) -> Box<dyn Fn(String) -> bool + 'a> {
    Box::new(
        move |filename: String| -> bool {
            // Regex pattern to find valid Rust identifiers.
            // In addition, it also accepts / (forward slash) in order to include the namespaces.
            let re: Regex = Regex::new(r"(?i)[a-z][a-z0-9_/]*|_[a-z0-9_/]+").unwrap();

            // The given expression is stored here as a mutable local variable.
            // This is done so that the plugin calls can be replaced with valid Rust function names,
            // which can then be evaluated.
            let mut text: String = expression.to_string();

            // Map plugin calls to temporary function names.
            let mut vars: HashMap<String, String> = HashMap::new();
            for (index, cap) in re.captures_iter(&text).enumerate() {
                // let boolean_str = if call_dispatcher(cap[1].to_string())(filename.to_string()) 
                //     { "true".to_string() } else { "false".to_string() } ;
                vars.insert(cap[0].to_string(), format!("var{}", index));
            }

            // Replace the plugin calls in the expression with the corresponding function name
            for x in &vars {
                text = text.to_string().replace(x.0, x.1)
            }

            // println!("Final expression: {}", text);

            // Create new Expr from the edited rule matcher expression
            let mut expression = Expr::new(text);

            // Insert values into Expr from the plugin calls
            for x in &vars {
                let value = call_dispatcher(x.0.to_string())(filename.to_string());
                // println!("{} {}", x.1, value);
                expression = expression.value(
                    x.1,
                    value)
            }

            // println!("expression: {:?}", expression);

            let result = expression.exec().unwrap();

            // println!("result: {}", result);

            return if result.is_boolean() {
                result.as_bool().unwrap()
            } else {
                false
            };
        }
    )
}

/// Function to retrieve a matcher dispatcher for a namesite
/// (Basically a wrapper for get_expression_matcher that satisfies the call_dispatcher argument)
///
///
/// # Arguments
///
/// * `namesite` - Namesite from which we access plugins
///
/// # Returns
///
/// A closure that takes a rule expression and returns a matcher closure for that expression
///
/// # Example
///
/// let matcher_dispatcher = get_matcher_dispatcher(namesite)
/// let expression = "!amazon/amazon_invoice && freddy/amazon"
/// let matcher = matcher_dispatcher(expression)
///
pub fn get_matcher_dispatcher<'a>(
    namesite: &'a Namesite,
) -> Box<dyn Fn(String) -> Box<dyn Fn(String) -> bool + 'a> + 'a> {
    Box::new(
        move |expression: String| -> Box<dyn (Fn(String) -> bool) + 'a>{
            let call_dispatcher = get_plugin_call_dispatcher(namesite);
            get_expression_matcher(expression, call_dispatcher)
        }
    )
}
