use std::path::PathBuf;
use std::fmt;
use std::io::Read;
use std::fs::File;

use nom::{
    IResult,
    named,
    opt,
    branch::alt,
    multi::{many0, many1, separated_list1},
    combinator::{eof, rest},
    sequence::{preceded, delimited, tuple},
    bytes::complete::{tag, is_not, take_until, take_till, take_while}
};

use crate::ns;

use crate::expr_rule::{get_expression_matcher, get_matcher_dispatcher};
use crate::expr_name::{evaluate_filename};

type Config = Vec<FolderBlock>;

#[derive(Debug)]
pub struct FolderBlock {
    pub name: String,
    pub paths: Vec<PathBuf>,
    pub recursive: bool,

    pub rules: Vec<Rule>
}

impl FolderBlock {
    //Return the first matching rule
    pub fn matching_rule(&self, path: PathBuf, root_ns: &ns::Namesite) -> Result<Option<&Rule>, &str> {

        for rule in &self.rules {
            match &rule.matches(path.clone(), root_ns) {
                Ok(did_match) => if *did_match { return Ok(Some(&rule)) },
                Err(e) => return Err(e)
            }
        }

        Ok(None)
    }
}

#[derive(Debug)]
pub struct Rule {
    pub name: String,
    pub matcher: String,
    pub filename: String
}

impl Rule {

    // Does this rule match the path?
    pub fn matches(&self, path: PathBuf, root_ns: &ns::Namesite) -> Result<bool, &str> {
        let matcher = get_matcher_dispatcher(root_ns)(self.matcher.clone());
        Ok(matcher(path.to_str().unwrap().to_string()))
    }

    //Apply the rule to the given path
    pub fn apply<'a>(&self, path: PathBuf, root_ns: &'a ns::Namesite) -> Result<(PathBuf, String), &'a str> {

        Ok((path.clone(), evaluate_filename(path.clone(), self.filename.clone(), root_ns)?))
    }
}

pub fn read_config(path: PathBuf) -> Result<Config, &'static str> {
    let mut file = File::open(path.as_path())
        .or(Err("Error opening file!"))?;

    let mut buffer = String::new();
    file.read_to_string(&mut buffer)
        .or(Err("Error reading file!"))?;

    match parse_config(&buffer) {
        Ok((_, config)) => Ok(config),
        Err(_) => Err("There was an error parsing the config")
    }
}

// type FolderBlockPaths = (Vec<PathBuf>, bool);
pub fn inside_parentheses(input: &str) -> IResult<&str, &str> {
    delimited(
        tag("("),
        take_until(")"),
        tag(")")
        )(input)
}

pub fn folder_block_header(input: &str) -> IResult<&str, (&str, Vec<PathBuf>, bool)> {
    let (input, n) = take_until("(")(input)?;
    let (input, paren) = inside_parentheses(input)?;
    
    //Comma separated paths:
    let (input2, cs_paths) = take_till(|c| c == ':')(paren)?;

    //This isn't that fancy tbh, 
    let paths: Vec<PathBuf> = cs_paths
        .split(',')
        .map(|c| PathBuf::from(c)).collect();

    let recursive = input2 == ":recursive";

    let (input, _) = tag(":\n")(input)?;

    Ok((input, (n, paths, recursive)))
}

pub fn folder_block_rule(input: &str) -> IResult<&str, Rule> {
    //"Bullet point" with which all the lines start with
    static bp: &str = "\t- ";
    let (input, _) = tag(bp)(input)?;
    let (input, name) = take_until("(")(input)?;
    
    let (input, rule_matcher) = inside_parentheses(input)?;
    let (input, _) = tag(":\n")(input)?;

    let (input, filename) = take_until("\n")(input)?;

    Ok((input, Rule {
        name: name.to_string(),
        matcher: rule_matcher.to_string(),
        filename: filename.to_string(),
    }))
}

pub fn parse_folder_block(input: &str) -> IResult<&str, FolderBlock> {
    let (input, (name, paths, recursive)) = folder_block_header(input)?;
    let (input, rules) = many1(folder_block_rule)(input)?;
    // let (input, rule)
    Ok((input, FolderBlock {
        name: name.to_string(),
        paths: paths,
        recursive: recursive,
        rules: rules
    }))
}

pub fn parse_config(input: &str) -> IResult<&str, Config> {
    let (input, blocks) = many0(parse_folder_block)(input)?;

    Ok((input, blocks))
}
