//fn main() {
//    println!("Hello, world!");
//    let opt = Cli::from_args();
    
//    match opt.plugins {
//        Some(s) => println!("There was a string: {}", s.to_str().unwrap()),
//        None => println!("There was no string"),
//    }

//    let s1 = &"Hello (~/Somewhere,.anotherewhere:recursive):\n";
//    println!("In: {}", s1);
//    println!("Parsed: {:?}", config::folder_block_header(s1).unwrap().1);

//    // println!("{}", config::read_config(PathBuf::from(".")).unwrap());
//    let s2 = "\t- Amazon Invoice (amazon/amazon_invoice):\n\t[invoice_date(yymmdd)]_[something]\n";
//    let result = config::folder_block_rule(s2).unwrap().1;
//    println!("\nIn: {}", s2);
//    println!("Parsed: {}, {}, {}", result.name, result.matcher, result.filename);

//    let s2 = &"Hello (~/Somewhere,.anotherewhere:recursive):
//\t- Amazon Invoice (amazon/amazon_invoice):\n\t[invoice_date(yymmdd)]_[something]
//\t- Amazon Invoice (amazon/amazon_invoice2):\n\t[something_else]_[something]\n";
    
//    println!("\nIn: {}", s2);
//    let res2 = config::parse_folder_block(s2).unwrap().1;
//    println!("Parsed: {:#?}", res2);


//    //Namespace stuff
//    let a_namespace = ns::Namesite::new_namespace(
//            "0",
//            None
//            );

//    let another_namespace = a_namespace.insert(ns::Namesite::new_namespace(
//            "1",
//            None));

//    let a_a_namespace = a_namespace.get("").unwrap().unwrap();
//    println!("a_a_namespace {}", a_a_namespace.name);

//    // let name = "2";
//    let smth = ns::Namesite::new_namespace(
//                "2",
//                None
//            );

//    let another_nested_namespace = another_namespace.unwrap().insert(smth);

//    let another_another_nested_namespace = another_nested_namespace.unwrap().insert(ns::Namesite::new_namespace(
//                "3",
//                Some("_2".to_string())
//            ));


//    fn test_callback<'a>(path: String, args: Vec<String>, preceding: Option<String>) -> pluglib::CommandReturnType<'a> {
//        return Ok("From inside the callback");
//    }

//    let (name, boxed) = pluglib::callback!(("test_callback", test_callback));
//    let the_callback = another_another_nested_namespace.unwrap().insert(ns::Namesite::new_callback(
//            &name,
//            boxed,
//            None));

//    // let found = a_namespace.get("1")
//    //     .unwrap().unwrap().get("2")
//    //     .unwrap().unwrap().get("3").unwrap();
    
//    //Get via nested:
//    let found = a_namespace.get("1/2/3").unwrap();

//    if let Some(x) = found {
//        println!("{}", x.name);

//        if let Some(y) = x.get("test_callback").unwrap() {
//            println!("{}", y.name);

//            if let ns::NamesiteKind::Command(smth) = &y.kind {
//                println!("{}", smth.call("hello".to_string(), vec!["1".to_string()], Some("".to_string())).unwrap());
//            }
//        }
//    }

//    let mut pl = loader::PluginLoader::new(PathBuf::from("./plugins")).unwrap();
//    let res = pl.load_plugins();
//    println!("{}", pl.loaded_plugins.len());
//}

//fn test_callback<'a>(path: String, args: Vec<String>, preceding: Option<String>) -> pluglib::CommandReturnType<'a> {
//    return Ok("From inside the callback");
//}

//pluglib::export_plugin!("aa", "aaa", pluglib::callback!(test_callback));
