use std::path::PathBuf;

extern crate pluglib;

pluglib::export_plugin!(
    "",
    "",
    pluglib::callback!(length),
    pluglib::callback!(reverse),
    pluglib::callback!(lowercase),
    pluglib::callback!(uppercase),
    pluglib::callback!(ext),

    pluglib::callback!(filename),

    pluglib::matcher!(matcher)
);

pub fn filename(_filename: String, _: Vec<String>, _: Option<String>) -> pluglib::CommandReturnType<'static> {
    Ok(PathBuf::from(_filename).file_name().unwrap().to_str().unwrap().to_string())
}

pub fn matcher(filename: String, args: Vec<String>, preceding: Option<String>) -> pluglib::CommandReturnType<'static> {
    Ok("true".to_string())
}

pub fn length(filename: String, args: Vec<String>, preceding: Option<String>) -> pluglib::CommandReturnType<'static> {
    Ok(preceding.unwrap().len().to_string())
}

pub fn reverse(filename: String, args: Vec<String>, preceding: Option<String>) -> pluglib::CommandReturnType<'static> {
    Ok(preceding.unwrap().chars().rev().collect::<String>())
}

pub fn lowercase(filename: String, args: Vec<String>, preceding: Option<String>) -> pluglib::CommandReturnType<'static> {
    Ok(preceding.unwrap().to_lowercase())
}

pub fn uppercase(filename: String, args: Vec<String>, preceding: Option<String>) -> pluglib::CommandReturnType<'static> {
    Ok(preceding.unwrap().to_uppercase())
}

pub fn ext(filename: String, args: Vec<String>, preceding: Option<String>) -> pluglib::CommandReturnType<'static> {
    Ok(filename.split(".").last().unwrap().to_string())
}
